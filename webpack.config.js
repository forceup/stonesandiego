const
    path = require('path'),
    webpack = require('webpack'),
    HtmlWebpackPlugin = require('html-webpack-plugin'),
    ExtractTextWebpackPlugin = require('extract-text-webpack-plugin'),
    CopyWebpackPlugin = require('copy-webpack-plugin'),
    OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin'),
    isProd = process.env.NODE_ENV === 'production',
    options = {
        entry: './src/js/app.js',
        output: {
            path: path.resolve(__dirname, 'dist'),
            filename: 'js/app.js'
        },
        module: {
            rules: [
                {
                    test: /\.scss$/,
                    use: ExtractTextWebpackPlugin.extract({
                        fallback: 'style-loader',
                        use: ['css-loader', 'postcss-loader', 'sass-loader'],
                        publicPath: '../'
                    })
                },
                // es6 to es5
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: 'babel-loader'
                },
                {
                    test: /\.pug$/,
                    use: [
                        {
                            loader: 'pug-loader',
                            options: {
                                pretty: isProd
                            }
                        }
                    ]
                },
                {
                    test: /\.(jpe?g|png|gif|svg)$/i,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                name: '[path][name].[ext]',
                                outputPath: 'img/',
                                context: './src/img'
                            }
                        },
                        'image-webpack-loader?bypassOnDebug'
                    ]
                },
                {
                    test: /\.(woff2?|eot|ttf)$/,
                    use: 'file-loader?name=fonts/[name].[ext]'
                }
            ]
        },
        devServer: {
            contentBase: path.join(__dirname, 'dist'),
            compress: true,
            hot: true,
            stats: 'errors-only',
            publicPath: '/'
        },
        plugins: [
            new webpack.HotModuleReplacementPlugin(),
            new HtmlWebpackPlugin({
                minify: false,
                template: './src/index.pug',
                filename: 'index.html'
            }),
          new HtmlWebpackPlugin({
            minify: false,
            template: './src/services.pug',
            filename: 'services/index.html'
          }),
          new HtmlWebpackPlugin({
            minify: false,
            template: './src/about.pug',
            filename: 'about-us/index.html'
          }),
          new HtmlWebpackPlugin({
            minify: false,
            template: './src/contact.pug',
            filename: 'contact-us/index.html'
          }),
          new HtmlWebpackPlugin({
            minify: false,
            template: './src/promotions.pug',
            filename: 'promotions/index.html'
          }),
          new HtmlWebpackPlugin({
            minify: false,
            template: './src/gallery.pug',
            filename: 'copy-of-services/index.html'
          }),
          new HtmlWebpackPlugin({
            minify: false,
            template: './src/marble.pug',
            filename: 'copy-of-travertine-new/index.html'
          }),
          new HtmlWebpackPlugin({
            minify: false,
            template: './src/travertine.pug',
            filename: 'copy-of-marble-new/index.html'
          }),
          new HtmlWebpackPlugin({
            minify: false,
            template: './src/limestone.pug',
            filename: 'copy-of-travertine-new-6/index.html'
          }),
          new HtmlWebpackPlugin({
            minify: false,
            template: './src/granite.pug',
            filename: 'copy-of-travertine-new-2/index.html'
          }),
            new ExtractTextWebpackPlugin({
                filename: 'css/style.css',
                disable: !isProd
            }),
          new CopyWebpackPlugin([
            { from: './src/php', to: 'php/' }
          ]),
          new CopyWebpackPlugin([
            { from: './src/img/static', to: 'img/' }
          ]),
            new OptimizeCssAssetsPlugin()
        ]
};

module.exports = options;
