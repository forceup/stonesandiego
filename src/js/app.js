import css from '../css/style.scss';
import $ from 'jquery';
import validate from 'jquery-validation';
require('fancybox')($);

function app() {

  // Form validation on a client
  $('.form').validate({
    submitHandler(f, e) {
      e.preventDefault();
      const t = $(f);
      t.addClass('loading');
      $.ajax({
        url: t.attr("action"),
        data: t.serialize(),
        type: 'post',
        success: function (data) {
          setTimeout(function() {
            t.removeClass('loading').addClass('success submitted');
          }, 1000);
        },
        error: function (data) {
          setTimeout(function() {
            t.removeClass('loading').addClass('fail submitted');
          }, 1000);
        }
      });
    }
  });

  // Image gallery
  $('[rel="gallery"]').fancybox({
      padding: 0
  });

}

window.addEventListener('load', app);